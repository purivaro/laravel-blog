require('./bootstrap');
// require('dropzone');
// window.Dropzone = require('dropzone');
require('bootstrap-fileinput');
require('bootstrap-fileinput/themes/fas/theme.js');
require('bootstrap-fileinput/js/locales/th.js');

require('select2');

window.trix = require('trix');

// window.flatpickr = require('flatpickr');
// import flatpickr from "flatpickr"
// import { Thai } from "flatpickr/dist/l10n/th.js"
window.flatpickr = require("flatpickr");
window.Thai = require("flatpickr/dist/l10n/th.js").default.th;

window.bootbox = require('bootbox');