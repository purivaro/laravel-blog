<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{mix('css/main.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/page.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="{{asset('assets/img/apple-touch-icon.png')}}">
    <link rel="icon" href="{{asset('assets/img/icon/dkcicon.png')}}">
    <style>
      .header {
        padding: 55px 0 30px 0;
      }
      .section {
        padding-top: 6rem;
        padding-bottom: 6rem;
      }
      .font-k2d {
        font-family: 'K2D', sans-serif;
      }
      .font-jamjuree {
        font-family: 'Bai Jamjuree', sans-serif;
      }
      .font-taviraj {
        font-family: 'Taviraj', sans-serif;
      }
      .font-trirong {
        font-family: 'Trirong', sans-serif;
      }
      .font-sarabun {
        font-family: 'Sarabun', sans-serif;
      }
      body {
        font-size: 1.1rem;
        font-family: 'Sarabun', sans-serif;
        /* font-family: 'Taviraj', serif; */
      }
      h1, h2, h3, h4, h5, h6 {
        font-family: 'Sarabun', sans-serif;
      }
      .sidebar hr {
        margin-top: 15px;
        margin-bottom: 15px;
      }
      @media (max-width: 991.98px) {
        .sidebar {
          padding-top: 0rem;
          padding-bottom: 0rem;
        }

        .section {
          padding-top: 2rem;
          padding-bottom: 3rem;
        }
      }

    </style>
    @yield('css')

  </head>

  <body>

    @include('layouts.frontend.navbar')

    @yield('header')

    @yield('main-content')

    @include('layouts.frontend.footer')

    <!-- Scripts -->
    <script src="{{asset('assets/js/page.min.js')}}"></script>
    <script src="{{asset('assets/js/script.js')}}"></script>

    @yield('scripts')

  </body>
</html>
