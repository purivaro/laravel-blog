<!-- Footer -->
<footer class="footer">
  <div class="container">
    <div class="row gap-y align-items-center">

      <div class="col-3 col-lg-3">
        <a class="navbar-brand text-light" href="{{route('frontend.index')}}">
          History072
        </a>
      </div>


      <div class="col col-lg-6">
        <div class="nav nav-uppercase nav-trim justify-content-lg-center">
          <span class="nav-link">© 2019 Developed by IBS CTDM</span>
        </div>
      </div>

    </div>
  </div>
</footer><!-- /.footer -->
