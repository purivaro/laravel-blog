@extends('layouts.frontend.main')

@section('title', 'Blogs')

@section('header')
  <!-- Header -->
  <header class="header text-center text-white" style="background-image: linear-gradient(-225deg, #5D9FFF 0%, #B8DCFF 48%, #6BBBFF 100%);">
        <div class="container">
  
          <div class="row">
            <div class="col-md-8 mx-auto">
  
              <h1>Latest Blog Posts</h1>
              <p class="lead-2 opacity-90 mt-6">Read and get updated on how we progress</p>
  
            </div>
          </div>
  
        </div>
      </header><!-- /.header -->
  
@endsection

@section('main-content')
  <!-- Main Content -->
  <main class="main-content">

    <section class="section bg-gray">
      <div class="container">

        <div class="row">
          <div class="col-md-8 col-xl-9">
            <div class="row gap-y">

              @forelse($posts AS $post)
                <div class="col-md-6 col-lg-4">
                  <div class="card d-block border hover-shadow-6 mb-6">
                  <a href="{{route('frontend.show', $post)}}"><img class="card-img-top" src="{{asset('storage/'.$post->image)}}" alt="Card image cap"></a>
                    <div class="p-6 text-center">
                      <p><a class="small-5 text-lighter text-uppercase ls-2 fw-400" href="{{route('frontend.show', $post)}}">{{$post->category->name}}</a></p>
                      <h5 class="mb-0"><a class="text-dark" href="{{route('frontend.show', $post)}}">{{$post->title}}</a></h5>
                    </div>
                  </div>
                </div>
              @empty
                <div class="col-md-6 col-lg-4">
                  <p>No Result found for query <strong>{{request()->query('search')}}</strong></p>
                </div>
              @endforelse

            </div>

            {{-- <nav class="flexbox mt-30">
                <a class="btn btn-white disabled"><i class="ti-arrow-left fs-9 mr-4"></i> Newer</a>
                <a class="btn btn-white" href="#">Older <i class="ti-arrow-right fs-9 ml-4"></i></a>
            </nav> --}}
            {{$posts->appends(['search'=>request()->query('search')])->links()}}

          </div>
          <div class="col-md-4 col-xl-3">
            @include('blog.frontend.partials.sidebar')
          </div>

        </div>
        


      </div>
    </section>

  </main>
  
@endsection