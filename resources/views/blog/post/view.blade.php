@extends('layouts.main')

@section('content')
  <div class="card">
    <img src="{{asset('storage/'.$post->image)}}" class="card-img-top">
    <div class="card-body">
      <h1>{{$post->title}}</h1>
      <h4>{{$post->description}}</h4>
      @if(isset($post->published_at))
        <p class="card-title">
          ({{$post->carbon_published_at()->format('l jS \\of F Y')}}) - 
          {{Carbon::createFromFormat('Y-m-d H:i:s',$post->published_at)->locale('th')->diffForHumans()}}
        </p>
      @endif
      <hr>
      <p>{!!$post->content!!}</p>
      <div class="mt-3"><a href="{{route('posts.index')}}" class="btn btn-primary">Back</a></div>
    </div>
  </div>
@endsection