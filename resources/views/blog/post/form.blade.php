@extends('layouts.main')


@section('error')
  @if($errors->any())
    @foreach($errors->all() AS $error)  
      <div class="alert alert-danger">{{$error}}</div>
    @endforeach
  @endif
@endsection

@section('content')
  <div class="card">
    <h4 class="card-header">{{isset($post)?'Edit':'Create'}} post</h4>
    <div class="card-body">
      <form action="{{isset($post)?route('posts.update', $post->id):route('posts.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        @if(isset($post))
          @method('PUT')
        @endif
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" name="title" id="title" class="form-control" value="{{old('title', isset($post)?$post->title:'')}}">
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <textarea type="text" name="description" id="description" class="form-control" >{{old('description', isset($post)?$post->description:'')}}</textarea>
        </div>
        <div class="form-group">
          <label for="content">Content</label>
          <input type="hidden" name="content" id="content" class="form-control" value="{{old('content', isset($post)?$post->content:'')}}">
          <trix-editor input="content"></trix-editor>
        </div>
        <div class="form-group">
          <label for="published_at">Published at</label>
          <input type="text" name="published_at" id="published_at" class="form-control" value="{{old('published_at', isset($post)?$post->published_at:'')}}">
        </div>
        <div class="form-group">
          <label for="category_id">Category</label>
          <select name="category_id" id="category_id" class="form-control">
            <option value="">-Choose-</option>
            @foreach($categories AS $category)
              <option value="{{$category->id}}" 
                @if(isset($post))
                  {{($category->id==$post->category_id)?"selected":""}}  
                @endif
              >{{$category->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="tags">Tags</label>
          <select name="tags[]" id="tags" class="form-control select2" multiple>
            <option value="">-Choose-</option>
            @foreach($tags AS $tag)
              <option value="{{$tag->id}}" 
                @if(isset($post))
                  {{$post->hasTag($tag->id)?"selected":""}}  
                @endif
              >{{$tag->name}}</option>
            @endforeach
          </select>
        </div>
        @if(isset($post))
          <div class="form-group">
            <img src="{{asset('storage/'.$post->image)}}" class="img-fluid">
          </div>
        @endif
        <div class="form-group">
          <label for="image">Image</label>
          <input type="file" name="image" id="image" class="form-control" value="{{old('image', isset($post)?$post->image:'')}}">
        </div>
        <button class="btn btn-primary">Save</button>
      </form>
    </div>
  </div>
@endsection

@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.1.0/trix.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.1.0/trix.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

  <script>
    document.getElementById('title').focus();
    flatpickr("#published_at", {
      enableTime: true,
      enableSeconds: true
    })

    $(function(){
      $(".select2").select2();
    })
  </script>
@endsection