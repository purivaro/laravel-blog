@extends('layouts.main')

@section('content')
  <div class="card">
    @foreach($dkc->images AS $image)
      <img class="card-img-top" src="{{asset($image->path)}}" >
    @endforeach
    <div class="card-body">
      <h1>{{$dkc->title}}</h1>
      <p class="mb-0"><a class="text-dark" href="{{route('frontend.show', $dkc)}}">{{$dkc->happened_on->locale('th')->isoFormat('dd D MMM YYYY')}}</a></p>
      <hr>
      <p>{!!$dkc->detail!!}</p>
      @if(isset($dkc->vdo))
        <h5><a href="{{$dkc->vdo}}" class="btn btn-danger" target='_blank'>VDO Link</a></h5>
      @endif
      @foreach($dkc->tags AS $tag)
        <a class="badge badge-pill badge-secondary" href="{{route('frontend.tag',$tag)}}">{{$tag->name}}</a>
      @endforeach
      <div class="mt-3"><a href="{{route('dkcs.index')}}" class="btn btn-primary">Back</a></div>
    </div>
  </div>
@endsection