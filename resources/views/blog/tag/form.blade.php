@extends('layouts.main')

@section('error')
  @if($errors->any())
    @foreach($errors->all() AS $error)  
      <div class="alert alert-danger">{{$error}}</div>
    @endforeach
  @endif
@endsection

@section('content')
  <div class="card">
    <div class="card-body">
      <form action="{{isset($tag)?route('tags.update', $tag->id):route('tags.store')}}" method="post">
        @csrf
        @if(isset($tag))
          @method('PUT')
        @endif
        <div class="form-group">
          <label for="name">{{isset($tag)?'Edit':'Create'}} Tag</label>
          <input type="text" name="name" id="name" class="form-control" value="{{isset($tag)?$tag->name:''}}">
        </div>
        <button class="btn btn-primary">Save</button>
      </form>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    document.getElementById('name').focus()
  </script>
@endsection