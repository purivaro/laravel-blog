<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
  public function send() {
    $data = ['title'=>'puri', 'content'=>'Hello'];
    Mail::to('aun.puri@gmail.com')->send(new Contact($data));
    return back();
  }
}
