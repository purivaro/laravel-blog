<?php

namespace App\Http\Controllers;

use App\Dkc;
use App\Post;
use App\Category;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FrontendController extends Controller
{
  private $months = [1 => 'ม.ค.', 2 => 'ก.พ.', 3 => 'มี.ค.', 4 => 'เม.ย.', 5 => 'พ.ค.', 6 => 'มิ.ย.', 7 => 'ก.ค.', 8 => 'ส.ค.', 9 => 'ก.ย.', 10 => 'ต.ค.', 11 => 'พ.ย.', 12 => 'ธ.ค.'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // $search = request()->query('search');
      // if($search){
      //   $posts = Post::where('title','LIKE',"%{$search}%")->simplePaginate(3);
      // }else{
      //   $posts = Post::simplePaginate(3);
      // }
      // return view('blog.frontend.index')
      //   ->withPosts($posts)
      //   ->withCategories(Category::all())
      //   ->withTags(Tag::all());
      // Dkc::orderBy('happened_on','desc')->paginate(10)
      // dd(Dkc::searched()->orderBy('happened_on','desc')->paginate(10));
      return view('blog.frontend.index')
        ->withDkcs(Dkc::searched()->orderBy('happened_on','desc')->paginate(5))
        // ->withDkcs(Dkc::searched()->latest()->paginate(5))
        ->withMonths($this->months)
        ->withTags(Tag::all());
    }


    public function show(Dkc $dkc)
    {
      // dd($dkc);
      return view('blog.frontend.show')
        ->withMonths($this->months)
        ->withDkc($dkc);
    }


    public function category(Category $category)
    {
      // $search = request()->query('search');
      // if($search){
      //   $posts = $category->posts()->where('title','LIKE',"%{$search}%")->simplePaginate(3);
      // }else{
      //   $posts = $category->posts()->simplePaginate(3);
      // }
      return view('blog.frontend.category')
        // ->withPosts($posts)
        ->withPosts($category->posts()->searched()->paginate(10))
        ->withTags(Tag::all())
        ->withCategories(Category::all())
        ->withCategory($category);
    }
    
    
    public function tag(Tag $tag)
    {
      // $search = request()->query('search');
      // if($search){
      //   $posts = $tag->posts()->where('title','LIKE',"%{$search}%")->simplePaginate(3);
      // }else{
      //   $posts = $tag->posts()->simplePaginate(3);
      // }      
      return view('blog.frontend.tag')
        // ->withPosts($posts)
        ->withDkcs($tag->dkcs()->searched()->paginate(5))
        ->withMonths($this->months)
        ->withTags(Tag::all())
        ->withTag($tag);
    }
    
    public function user(User $user)
    {
    
      return view('blog.frontend.user')
        // ->withPosts($posts)
        ->withUser($user)
        ->withPosts($user->posts()->searched()->paginate(5))
        ->withTags(Tag::all())
        ->withCategories(Category::all());
    }

}
