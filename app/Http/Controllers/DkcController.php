<?php

namespace App\Http\Controllers;

use App\Dkc;
use App\Tag;
use App\DkcImage;
use Illuminate\Http\Request;
use App\Http\Requests\Dkcs\FormHistoryRequest;

class DkcController extends Controller
{
  private $months = [1 => 'ม.ค.', 2 => 'ก.พ.', 3 => 'มี.ค.', 4 => 'เม.ย.', 5 => 'พ.ค.', 6 => 'มิ.ย.', 7 => 'ก.ค.', 8 => 'ส.ค.', 9 => 'ก.ย.', 10 => 'ต.ค.', 11 => 'พ.ย.', 12 => 'ธ.ค.'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $months = [1 => 'ม.ค.', 2 => 'ก.พ.', 3 => 'มี.ค.', 4 => 'เม.ย.', 5 => 'พ.ค.', 6 => 'มิ.ย.', 7 => 'ก.ค.', 8 => 'ส.ค.', 9 => 'ก.ย.', 10 => 'ต.ค.', 11 => 'พ.ย.', 12 => 'ธ.ค.'];
      return view('blog.dkc.index')
        ->withDkcs(Dkc::searched()->orderBy('happened_on','desc')->paginate(10))
        ->withMonths($this->months);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('blog.dkc.form')
        ->withTags(Tag::all())
        ->withMonths($this->months);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormHistoryRequest $request)
    {
      $attribute = $request->all();
      $attribute['user_id'] = auth()->user()->id;
      $y = $request->y-543;
      $attribute['happened_on'] = "{$y}-{$request->m}-{$request->d}";

      $dkc = Dkc::create($attribute);

      if(isset($request->images)){
        foreach($request->images AS $reqimage)
        {

          $image = $reqimage->store('dkcs');

          $dkcimg = DkcImage::create([
            "dkc_id"=>$dkc->id,
            "path"=>$image,
          ]);
        }
      }

      if($request->tags){
        $dkc->tags()->attach($request->tags);
      }

      return redirect(route('dkcs.index'))->withStatus('เพิ่มข้อมูลเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dkc  $dkc
     * @return \Illuminate\Http\Response
     */
    public function show(Dkc $dkc)
    {
      return view('blog.dkc.view')->withDkc($dkc);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dkc  $dkc
     * @return \Illuminate\Http\Response
     */
    public function edit(Dkc $dkc)
    {
      return view('blog.dkc.form')
      ->withDkc($dkc)
      ->withTags(Tag::all())
      ->withMonths($this->months);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dkc  $dkc
     * @return \Illuminate\Http\Response
     */
    public function update(FormHistoryRequest $request, Dkc $dkc)
    {
      $attribute = $request->all();
      $attribute['user_id'] = auth()->user()->id;
      $y = $request->y-543;
      $attribute['happened_on'] = "{$y}-{$request->m}-{$request->d}";
      $dkc->update($attribute);

      if(isset($request->images)){

        $DkcImages = DkcImage::where('dkc_id', $dkc->id)->get();
        // dd($DkcImages);
        foreach($DkcImages AS $DkcImage){
           $DkcImage->deleteImage(); // ลบรูป
           $DkcImage->delete(); // ลบ record
        }
       
        // DkcImage::where('dkc_id', $dkc->id)->delete();

        foreach($request->images AS $reqimage)
        {

          $image = $reqimage->store('dkcs');

          $dkcimg = DkcImage::create([
            "dkc_id"=>$dkc->id,
            "path"=>$image,
          ]);
        }
      }

      if($request->tags){
        $dkc->tags()->sync($request->tags);
      }

      return redirect(route('dkcs.index'))->withStatus('แก้ไขข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dkc  $dkc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dkc $dkc)
    {
      $DkcImages = DkcImage::where('dkc_id', $dkc->id)->get();
      // dd($DkcImages);
      foreach($DkcImages AS $DkcImage){
         $DkcImage->deleteImage(); // ลบรูป
         $DkcImage->delete(); // ลบ record
      }
      $dkc->delete();
      return redirect(route('dkcs.index'))->withStatus('ลบข้อมูลเรียบร้อย');
    }


    public function upload(Request $request)
    {
      $path = $request->file->store('dkcs');
      return $path;
    }
}
