<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Requests\Posts\CreatePostsRequest;
use App\Http\Requests\Posts\UpdatePostsRequest;
use Illuminate\Support\Facades\Storage;
class PostController extends Controller
{
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('blog.post.index')->with('posts', Post::paginate(3));
    }

    public function welcome()
    {
      return view('welcome')
        ->withPosts(Post::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      return view('blog.post.form')
        ->with('categories', Category::all())
        ->withTags(Tag::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostsRequest $request)
    {
      $image = $request->image->store('posts');
      $attribute = $request->all();
      $attribute['image'] = $image;
      $attribute['user_id'] = auth()->user()->id;
      // dd($attribute);
      $post = Post::create($attribute);

      if($request->tags){
        $post->tags()->attach($request->tags);
      }

      return redirect(route('posts.index'))->with('status', 'Post Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
      return view('blog.post.view')->withPost($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
      return view('blog.post.form')
        ->with('post', $post)
        ->with('categories', Category::all())
        ->withTags(Tag::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostsRequest $request, Post $post)
    {
      $data = $request->all();      
      if($request->hasFile('image')){

        $image = $request->image->store('posts');

        //Storage::delete($post->image);
        $post->deleteImage();

        $data["image"] = $image;
      }
      $post->update($data);

      if($request->tags){
        $post->tags()->sync($request->tags);
      }

      return redirect(route('posts.index'))->withStatus('Update Successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Post $post)
    // {
    //   $post->delete();
    //   return redirect(route('posts.index'))->with('status', 'Delete Successful');
    // }

    public function destroy($id)
    {
      $post = Post::withTrashed()->where('id', $id)->firstOrFail();
      if($post->trashed()){
        //Storage::delete($post->image);
        $post->deleteImage();

        $post->forceDelete();
      }else{
        $post->delete();
      }
      return redirect(route('posts.index'))->with('status', 'Delete Successful');
    }


    public function trashed(){
      $trashed = Post::onlyTrashed()->get();
      return view('blog.post.trashed')->withTrashedPosts($trashed);
    }

    public function restore($id){
      $post = Post::withTrashed()->where('id', $id)->first();
      $post->restore();
      return redirect(route('posts.index'))->withStatus('Restore Successful');
    }



}
