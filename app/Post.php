<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class Post extends Model
{
  use SoftDeletes;

  protected $dates = [
    'published_at'
  ];
  
  protected $fillable = [
    'title',
    'description',
    'content',
    'image',
    'published_at',
    'user_id',
    'category_id',
  ];

  /*
   Delete Image
  */
  public function deleteImage()
  {
    Storage::delete($this->image);
  }

  public function category()
  {
    return $this->belongsTo(Category::class);
  }

  public function tags()
  {
    return $this->belongsToMany(Tag::class);
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function hasTag($tagId)
  {
    return in_array($tagId, $this->tags->pluck('id')->toArray());
  }

  public function carbon_published_at()
  {
    return Carbon::createFromFormat('Y-m-d H:i:s', $this->published_at)->locale('th');
  }


  public function scopeSearched($query)
  {
    $search = request()->query('search');

    if(!$search){
      return $query->published();
    }

    return $query->published()->where('title', 'LIKE', "%{$search}%");

  }

  public function scopePublished($query)
  {
    return $query->where('published_at', '<=', now());
  }

  // เพื่อดัดแปลงค่าจากฟิลด์ image ก่อนออกมา ด้วยการเติมชื่อโฟลเดอร์ไปข้างหน้า
  public function getImageAttribute($value)
  {
    return 'storage/'.$value;
  }
}
